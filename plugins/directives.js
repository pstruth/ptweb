import Vue from 'vue'

/***
 * Directive to handle the scroll more easily in our components by v-scroll
 */
Vue.directive('scroll', {
  inserted (el, binding) {
    const f = function (evt) {
      if (binding.value(evt, el)) {
        window.removeEventListener('scroll', f)
      }
    }
    window.addEventListener('scroll', f)
  }
})
