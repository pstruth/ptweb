import axios from 'axios'

/**
 * Fetch images from https://jsonplaceholder.typicode.com/photos
 */
const fetchImages = async (callback) => {
  try {
    const data = await axios.get('https://jsonplaceholder.typicode.com/photos')
    callback(null, data.data)
  } catch (err) {
    callback(err, null)
  }
}

export default fetchImages
