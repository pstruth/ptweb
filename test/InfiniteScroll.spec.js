import { shallowMount } from '@vue/test-utils'
import BootstrapVue from 'bootstrap-vue'
import Vue from 'vue'
import FakeImagesForTesting from '../FakeImagesForTesting'
import InfiniteScroll from '@/components/infinitescroll/InfiniteScroll.vue'
Vue.use(BootstrapVue)
require('../plugins/directives')

// Factory para obtener el componente con props
function getInfiniteScroll (propsData) {
  return shallowMount(InfiniteScroll, { propsData })
}

describe('InfiniteScroll', () => {
  const imagesTestJSON = FakeImagesForTesting

  test('Component mounted correctly', () => {
    const component = getInfiniteScroll({ images: imagesTestJSON })
    expect(component).toBeTruthy()
  })

  test('Limit works correctly (40)', () => {
    const wrapper = getInfiniteScroll({ images: imagesTestJSON })
    expect(wrapper.vm.imagesDisplayed.length).toBe(50)
  })

  test('Remove feature when click in image wrapper', () => {
    const wrapper = getInfiniteScroll({ images: imagesTestJSON })
    expect(wrapper.vm.imagesDisplayed.length).toBe(50)

    wrapper.find('.img-wrapper').trigger('click')
    expect(wrapper.vm.imagesDisplayed.length).toBe(49)
  })

  test('Load more images when scrolled to bottom', () => {
    const wrapper = getInfiniteScroll({ images: imagesTestJSON })
    expect(wrapper.vm.imagesDisplayed.length).toBe(50)

    window.dispatchEvent(new CustomEvent('scroll', { detail: 2000 }))

    expect(wrapper.vm.imagesDisplayed.length).toBeGreaterThan(50)
  })
})
