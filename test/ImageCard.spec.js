import { shallowMount } from '@vue/test-utils'
import ImageCard from '@/components/infinitescroll/ImageCard.vue'
const localVue = global.localVue

// Factory para obtener el componente con props
function getImageWithData (propsData) {
  return shallowMount(ImageCard, { propsData, localVue })
}

describe('ImageCard', () => {
  test('Component mounted correctly', () => {
    const component = getImageWithData({ image: { url: 'https://via.placeholder.com/150', title: 'image' } })
    expect(component).toBeTruthy()
  })

  test('Src of ImageCard is the url from image prop', () => {
    const wrapper = getImageWithData({ image: { url: 'https://via.placeholder.com/150', title: 'image' } })
    expect(wrapper.find('.image-card').attributes().src).toBe('https://via.placeholder.com/150')
  })

  test('Alt of ImageCard is the title from prop', () => {
    const wrapper = getImageWithData({ image: { url: 'https://via.placeholder.com/150', title: 'image' } })
    expect(wrapper.find('.image-card').attributes().alt).toBe('image')
  })

  test('Change loaded when Load image', () => {
    const wrapper = getImageWithData({ image: { url: 'https://via.placeholder.com/150', title: 'image' } })
    wrapper.vm.onLoad()
    expect(wrapper.vm.loaded).toBeTruthy()
  })
})
