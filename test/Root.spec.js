import { shallowMount } from '@vue/test-utils'
import Vue from 'Vue'
import flushPromises from 'flush-promises'
import FakeImagesForTesting from '../FakeImagesForTesting'
import Landing from '@/pages/index.vue'
const localVue = global.localVue
jest.mock('axios')
// Factory para obtener el componente con props
function getImageWithData (propsData) {
  return shallowMount(Landing, { propsData, localVue })
}

describe('Landing', () => {
  test('Component mounted correctly', () => {
    const component = getImageWithData({ })
    expect(component).toBeTruthy()
  })

  test('Test fecth method', async () => {
    const component = getImageWithData({ })
    await flushPromises()
    await Landing.fetch.call(component.vm)
    expect(component.vm.images).toBeTruthy()
    expect(component).toBeTruthy()
  })

  test('Show infinite scroll only when images loaded', async () => {
    const wrapper = getImageWithData({ })
    expect(wrapper.find('#infinite-scroll-wrapper').exists()).toBe(false)
    wrapper.setData({
      images: FakeImagesForTesting
    })
    await Vue.nextTick()
    expect(wrapper.find('#infinite-scroll-wrapper').exists()).toBe(true)
  })
})
