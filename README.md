# Prueba técnica

## Alcance

Este proyecto corresponde a la prueba técnica del proceso de selección. Se tenían que cumplir con estos criterios:

### `US (historia de usuario)`

```bash
As a user visiting the root URL
I can see a grid of images that fill the screen on load
When I scroll the browser, I see more images loaded
When I click an image It is removed from the list and disappears
```

### `Adicionalmente`

```bash
JavaScript should be used as a platform.
You must use this placeholder URL for getting the images: https://jsonplaceholder.typicode.com/photos
The grid should be responsive, showing 2 columns on small devices and more as the device width growths
The images can’t pop or flicker, they must appear smoothly on the screen.
Include tests is a plus.
```

## Correr tests y la aplicación

Con Nuxt podemos crear tanto una versión para levantarla en un puerto (por defecto el 3000) como generar un directorio estático. En este caso dejo la instrucción para levantarla en un puerto que para probar es lo más sencillo.

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev
```

Para correr los tests bastará con ejecutar:
```bash
# install dependencies (en caso de no haberlo hecho)
$ npm install

# serve with hot reload at localhost:3000
$ npm run test
```


